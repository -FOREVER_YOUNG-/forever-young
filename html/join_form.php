﻿<!--
# 아이디 ☞ 영어소문자, 영어대문자, 숫자만 입력 가능하게 해주세요.
# 비밀번호  ☞  영어소문자, 영어대문자, 숫자, 특수기호가 모두 들어가게 해주세요.
# 이름   ☞  한글만 입력 가능하게 해주세요.
              ☞  2~5글자까지만 입력 가능하게 해주세요.
# 전화번호   ☞ 000-0000-0000 형식을 무조건 지키게 해주세요.
# 이메일   ☞  이메일 형식을 무조건 지키게 해주세요.
-->
<html>
	<head>
		<title>
		회원가입폼만들기
		</title>
	</head>
	<body>
		<form action="join_login.php" method="post">
			회원가입 정보입력<br>
			이름<input pattern ="[가-힣]*" minlength=2 maxlength=5 name="name" required><br>
			아이디<input pattern ="[a-zA-z0-9]*" name="id" required><br>
			비밀번호<input pattern ="[a-zA-z0-9?!@#]*" name="pw" required><br>
			전화번호<input pattern ="^\d{3}-\d{3,4}-\d{4}$" name="phone_number" required><br>
			이메일<input pattern="^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$" name="email" required><br>
			<button type="submit">전송</button>
		</form>
	</body>
</html>