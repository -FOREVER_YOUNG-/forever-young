﻿<html>
	<head>	
		<title>
			커피/음료
		</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<style>
			body{
				padding-left:5%;
				padding-right:5%;
				padding-top:0%;
								
			}
			
			.header{
				text-align:center; 
			}
			
			.footer{
				display:flex;
				justify-content:center;
				align-items:center;
				height:15%;
			
			}
			
			.mainpage{
				padding-left:30px;
				padding-right:30px;

			}
			.div_recommend{
				background-color:none;
				display:inline-block;
			}
			
			.span_element{
				display:inline-block;
				justify-content:center;
				align-items:center;
				width:320px;
				height:380px;
				background-color:none;
				margin-top:10px;
				padding:10px;
				text-align:center;
				margin-right:15px;
				vertical-align: top; <!--최상단에 줄맞추기위한 -->
			
			}
			.span_element:hover{
				background-color:#eee;
				box-shadow:2px 2px 2px 2px #eee
			}
			.span_element2{
				display:inline-block;
				justify-content:center;
				align-items:center;
				width:420px;
				height:520px;
				background-color:#eee;
				margin-top:10px;
				margin-right:40px;
				padding:10px;
				vertical-align: top; <!--최상단에 줄맞추기위한 -->
			}
			.span_element2:hover{
				background-color:skyblue;
			}
			.span_element3{
				display:inline-block;
				justify-content:center;
				align-items:center;
				width:200px;
				height:200px;
				background-color:orange;
				margin-top:10px;
				text-align:center;
				vertical-align: top; <!--최상단에 줄맞추기위한 -->
			}
			
					#menubar ul{
				margin:0;
				
			}
		
			#menubar ul li{
				display:inline-block;
				list-style-type:none;
				padding-top:1%;
				padding-bottom:1%;
				padding-left:2%;
				padding-right:2%;
				margin:0%;

			 }
			#menubar1 ul li{
				display:inline-block;
				list-style-type:none;
				padding-top:1%;
				padding-bottom:1%;
				padding-left:0.8%;
				padding-right:0.8%;
				margin:0%;

			 }
			 
			 #menubar1 ul li a{
				margin:0;
				color:black;
				
			}
			
			 #menubar ul li a{
				color:BLACK; <!--보이는 색깔-->
				text-decoration:none;<!--마우스를 올렸을때 밑줄을 그을 껀지 아닐지 디폴트는 그음-->
				
			 }
			 #menubar ul li a:hover{
				color:violet;
				<!--글자가 진하게 보였으면 좋겠어-->
			 }
		
			 <!--드랍다운 공부-->
		    ul, ol, li { list-style:none; margin:0; padding:0; }
		   
		
			ul.myMenu {}
			ul.myMenu > li { 
				display:inline-block;  
				background:#eee; 
				border:1px solid #eee; 
				text-align:center; 
				position:relative; 
			}
			
			ul.myMenu > li:hover { 
				background:#fff; 
			}
				
			ul.myMenu > li ul.submenu { 
				display:none; 
				position:absolute; 
				top:30px; 
				left:0; 
			}
			
			ul.myMenu > li:hover ul.submenu { 
				display:block; 
			}
			
			ul.myMenu > li ul.submenu > li { 
				display:inline-block; 
				width:100%; 
				padding:5px 10px; 
				background:#eee; 
				border:1px solid #eee; 
				text-align:center; 
			}
			ul.myMenu > li ul.submenu > li:hover { 
			background:#fff; }
			
			
			<!--라디오 버튼을 위한 css-->
	
		
		<!--버튼 css-->
		.button_css{
			background-color:orange;
			border-radius:10px;
		}
		.button_css:hover{
			background-color:orange;
		}
		
		input[type=submit]{
				vertical-align:top;
				<!--width:150px;
				height:60px;
				background-color:#eee;
				border:#eee;
				text-align:center;
				font-size:1.5em;
				padding-top:11px;
				color:black;
				display:inline-block;
				-->
				
				display:inline-block;width:150px;height:60px;background-color:#eee;font-size:1.5em;text-align:center;padding-top:3px;color:black
			}
			
			<!--radio타입 css-->
			input[type="radio"]{
				display:none;
			}
			
			#ice input[type="radio"]:checked{
			  display:inline-block;
			  background:none;
			  border:1px solid #dfdfdf;  
			  padding:0px 10px;
			  text-align:center;
			  height:35px;
			  line-height:33px;
			  font-weight:500;
			  cursor:pointer;
			}

			#ice input[type="radio"]:checked + span{
			  border:1px solid #23a3a7;
			  background:#23a3a7;
			  color:#fff;
			}
			
			</style>
		<script src ="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script>
	
			var storage=1;
			function plus(){
				if(storage<=5){
					document.getElementById("quantity").innerHTML=storage+1;
					document.getElementById("quantity").value=storage+1;
					storage++;
					document.getElementById("total_price").innerHTML=4100*storage;
					document.getElementById("total_price").value=4100*storage;
				}
			}
			function minus(){
				if(storage!=1){
					document.getElementById("quantity").innerHTML=storage-1;
					document.getElementById("quantity").value=storage-1;
					storage--;
				
					document.getElementById("total_price").innerHTML=4100*storage;
					document.getElementById("total_price").value=4100*storage;
				}
			}
		function bag_check_login(){ //장바구니로 연결하기전 로그인 여부 확인 
				$.post("http://101.101.218.121/project_mart_controller/bag_01", //bag_01에서는 session을 모조리 다 저장 
					{
					"drink_name":document.getElementById("drink_name").value,
					"ice":document.getElementById("ice").value,
					"add_shot":document.getElementById("add_shot").value,
					"water":document.getElementById("water").value,
					"whipping":document.getElementById("whipping").value,
					"add_price":document.getElementById("add_price").value,
					"quantity":document.getElementById("quantity").value,
					"temp":document.getElementById("temp").value,
					"total_price":document.getElementById("total_price").value
					},
					function (data){ 
						if(data==0)
						document.location.href="http://101.101.218.121/project_mart_controller/bag_02"; //view파일로 연결 
						
						else if(data==1){
							alert("로그인후 이용가능합니다");
							location.href="http://101.101.218.121/project_mart_controller/login";
						}
						

					}
				);
			}
			function order_check_login(){
				$.post("http://101.101.218.121/project_mart_controller/finish_order",
					{
					"drink_name":document.getElementById("drink_name").value,
					"ice":document.getElementById("ice").value,
					"add_shot":document.getElementById("add_shot").value,
					"water":document.getElementById("water").value,
					"whipping":document.getElementById("whipping").value,
					"add_price":document.getElementById("add_price").value,
					"quantity":document.getElementById("quantity").value,
					"temp":document.getElementById("temp").value,
					"total_price":document.getElementById("total_price").value
					},
					function (data){ 
						if(data==0)
						document.location.href="http://101.101.218.121/project_mart_controller/order_detail_view";//finish_order_02로 입력내용을 한번더 출력시키려했으나 그냥 바로 뛰어넘고 주문자 정보를 입력받음
						
						else if(data==1){
							alert("로그인후 이용가능합니다");
							location.href="http://101.101.218.121/project_mart_controller/login";
						}
						

					}
				);
			}
		</script>
		
	</head>
	<body>
		<p>
			<div class="header">
				<p><span style="font-size:3em;background-color:none;"><a href="http://101.101.218.121/project_mart_controller/start"dk style="text-decoration:none;color:black"><strong>커피장사</strong></a></span></p>
				<span style="font-size:1em;background-color:none;padding-right:10%;padding-left:10%; padding-top:0.3%;padding-botton:0.3%">
					커피 맛과 향, 커피장수에 의해 결정되노라
				</span>
			</div>
		</p>
		<hr>
		
		<div id="menubar" style="font-size:1.2em; text-align:left padding-left:0;background-color:none">
			<ul class="myMenu" style="font-align:center;background-color:#eee" >
					<li class="menu1"><a href="http://101.101.218.121/project_mart_controller/start"><strong>HOME</strong></a></li>
					
					<li class="menu2"><a href="http://101.101.218.121/project_mart_controller/coffeemenu">커피 and 음료</a></li>
						
					<li class="menu4"><a href="http://101.101.218.121/project_mart_controller/breadmenu">디저트</a></li>
					<li class="menu5"><a href="http://101.101.218.121/project_mart_controller/cakemenu">케이크</a></li>
					<li><a href="http://101.101.218.121/project_mart_controller/load_view">오시는 길</a></li>
			</ul>
		
		</div><br>
	
			<input type="hidden" name="drink_name" id="drink_name" value="아메리카노">
			<input type="hidden" name="ice" value="얼음량 기본" id="ice">
			<input type="hidden" name="add_shot" value="1" id="add_shot">
			<input type="hidden" name="water" value="물량 기본" id="water">
			<input type="hidden" name="whipping" value="휘핑없음" id="whipping">
			<input type="hidden" name="add_price" value="0" id="add_price">
	
		<div class="mainpage">
			
				<div style="font-size:2.7em">아메리카노<br></div><hr>
				<div style="display:inline-block;margin-top:20px">
					<img src="http://101.101.218.121/아아.jpg" width=700 height =700>
				</div>
				
			<div style="display:inline-block;vertical-align:top;margin-left:50px;margin-top:20px;">
				<div style="font-size:3em;padding-right:400px">
					<span><strong>4,100원</strong></span><br><br>
				</div>
				
				<!--주문하기-->
				<div style="font-size:20pt;background-color:none"><strong>주문하기</strong></div>
				<hr>
				
				<div style="font-size:20pt;">
					<span><a href="javascript:minus();" style="background-color:skyblue;padding-left:10pt;padding-right:10pt;font-size:1em;">-</a></span>
					<span input type="text" id="quantity" name="quantity" value="1">1</span>
					<span><a href="javascript:plus();"  style="background-color:skyblue;padding-left:10pt;padding-right:10pt;font-size:1em;">+</a></span>
				</div><br>
				
				<div style="font-size:20pt;">	
					<label>
						<input type="radio" name="temp" id="temp" value="ice" checked="checked">
							<span style="font-size:20pt;color:blue;border:blue;">ICE</span>
					</label>
					<label>
						<input type="radio" name="temp" value="hot">
							<span style="font-size:20pt">HOT</span>
					</label>
				
				</div><br>
			
				<div style="font-size:1.5em">
					<a href="#" onclick="window.open('http://101.101.218.121/project_mart_controller/option_view','옵션선택창','resizable=no width=600 height=500');return false">퍼스널옵션선택하기</a>
				</div><br>
			
				<hr>
				<div style="font-size:20pt;">
					<span><strong>합계:</strong></span>
					<span input type="text" id="total_price" name="total_price" value="4100">4100</span>
					<span>원</span>
					
				</div><br>
				<div>
				<span style="text-align:center;display:inline-block"><input type="submit" style="align:center;text-align:center;width:200px" value="장바구니" onclick="bag_check_login();"></span>
				<span style="text-align:center;display:inline-block"><input type="submit" style="align:center;text-align:center;width:200px" value="주문하기" onclick="order_check_login();"></span>
				</div>
			</div>
		
		<hr>
		<div class="footer" style="background-color:none;">커피장사를 찾아주셔셔 감사합니다</div>
	</div>

	</body>
</html>