﻿<html>
	<head>
		<title>
			로그인창
		</title>
		<script src ="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		
		<script>
			function login(){ //로그인을 실행하는 함수
				$.post(
					"http://101.101.218.121/session_login_model_controller/login",  //로그인을 시행을 돕는 model을 이어주기위한 컨트롤러가 받아야해
					{id:document.getElementById("id").value,
					 pw:document.getElementById("pw").value}, //입력 id를 가져와 id의 값으로 넘긴다
					function(data){  //data를 넘겨서 data값에 따른 출력
						if(data="로그인실패")
							alert(data);
					}
				);
			}

			function logout(){
				$.post(
					"http://101.101.218.121/session_login_model_controller/logout",
					function(data){
						alert(data);
					
					}
				);
			}
			
			function out(){
				document.getElementById("id").value="";
				document.getElementById("pw").value="";
			}
		
			
		</script>
	</head>
	<body>
		로그인창<br><br>
		
		아이디
		<input type="text" name="id" id="id"><br>
		비밀번호
		<input type="text" name="pw" id="pw">
		<button type="button" onclick ="login()">로그인</button><br>
		
		<form action="http://101.101.218.121/session_login_model_controller/join" method="post">
			<button type="submit">회원가입</button>
		</form>
		
		<button type="button" onclick ="javascript:out();">취소</button>
		<button type="button" onclick ="logout()">로그아웃</button>
	</body>
</html>