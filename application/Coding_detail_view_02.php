﻿<!doctype html>
<html>
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>
		</title>
		<!-- 합쳐지고 최소화된 최신 CSS -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
		<style>
			
		</style>
		
		<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>

		<script>

			$(document).ready(function(){

				$('#btn1').click(function(){

					var offset = $('#div1').offset(); //선택한 태그의 위치를 반환

						//animate()메서드를 이용해서 선택한 태그의 스크롤 위치를 지정해서 0.4초 동안 부드럽게 해당 위치로 이동함 

					$('html').animate({scrollTop : offset.top}, 400);

				});

			});

		</script>

	</head>

	<body>
		<!--규칙 큰 항목 별은 br을 2개쓰고 작은 글씨 사이에서는 br을 한번 네모칸안에 있는 아이들끼리는 br을 안씀-->
		<div class = " container-fluid ">
		<!--하단에 고정된 구매하기 버튼-->
		<nav class="navbar fixed-bottom navbar-light bg-light" style="width:70%;margin:0 auto">
			<div class="container-fluids" >
				<a class="navbar-brand" href="#">Fixed bottom</a>
			</div>
		</nav>


			<!--사진과 설명이 있는 베너-->
			<div class = " row justify-content-center " style="height:400px">
				<div class = "col-xs-12 col-sm-12 col-md-12 col-lg-6 " style="background-color:#f0f0f0"></div>
				<div class = "col-xs-12 col-sm-12 col-md-12 col-lg-6 " style="background-color:#d6d6d6"></div>
			</div>
			
			
			<!--상세페이지 메뉴바에 대한 nav, 반응성 수정-->
			<nav class="navbar sticky-top navbar-expand-lg navbar-light bg-light" style ="width:100%;margin:20px auto "> <!--width로 nav길이 조절-->
					
					<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					  <span class="navbar-toggler-icon"></span>
					</button>
				<div class="container-fluid"> <!--container속성 양쪽 정렬이나 중앙정렬로 변경-->
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav">
							<li class="nav-item">
								<a class="nav-link active" aria-current="page" href="#">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Features</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Pricing</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
							</li>
						</ul>
					</div>
			  </div>
			</nav>
			<!--
			<button id="btn1">div1로 이동</button>
			<button id="btn2">div2로 이동</button>
			<button id="btn2">div2로 이동</button>
			
			<div id="div1">div1</div>
			<div id="div2">div1</div>
			<div id="div3">div1</div>
			-->
			<div class="row justify-content-center">
				<div class="col-auto">
					<h4 style="text-decoration:underline;"><strong>강의소개</strong></h4>   
				</div>
			</div>
			<br>
			<br>
			<br>
			<div class="row justify-content-center">
				<div class="col-auto">
					<h4><strong>반드시 필요한 선수지식</strong></h4>
				</div>
			</div>
			<br>
			<div class="row justify-content-center">
				<div class="col-auto">
					<h6><strong>반드시 아래 지식을 가지고 있어야 가능합니다.</strong></h6>
				</div>
            </div>
			<br>
			<div class="row justify-content-center">
				<div class=" col-sm-0 col-md-0 col-lg-2" style="background-color:none;"><!--반응성을 여백이 작아지는 걸로 하자-->
				</div>
				<div class=" col-sm-12-auto col-md-10-auto col-lg-8" style="background-color:#f0f0f0;padding:30px">
					 <h5 style="text-align:center"><strong>HTML,CSS,자바스크립트에 대한 지식을 갖추고 있어야 합니다.</strong></h5>
				   <h6  style="text-align:center" ><strong>본 강좌는 "웹" 애플리케이션을 개발합니다. <br>따라서 위 세가지 기술에 대한 이해가 없이는 본 강좌를 학습하는 것이 불가능합니다.</strong></h6>
				</div>
				<div class="col-sm-0 col-md-0 col-lg-2" style="background-color:none;">
				</div>
            </div>
			<br>
			
			<br>
			<br>
			<div class="row justify-content-center">
				<div class="col-auto">
					<h4><strong>이 강의에서 배우는 것들</strong></h4>
				</div>
			</div>
			<br>
			<div class="row justify-content-center">
				<div class="col-auto">
					<h6><strong>대부분의 웹 애플리케이션이 기본으로 갖추고 있는 기능을 구현</strong></h6>
				</div>
            </div>
			<br>
			<div class="row justify-content-center">
				<div class=" col-sm-0 col-md-0 col-lg-2" style="background-color:none;"><!--반응성을 여백이 작아지는 걸로 하자-->
				</div>
				<div class=" col-sm-12-auto col-md-10-auto col-lg-8" style="background-color:#f0f0f0;padding:30px;padding-left:50px;padding-right:50px">
					 <h6 style="text-align:left"><strong>1. 회원가입, 로그인, 로그아웃 등 홈페이지의 기본기능을 배웁니다.</strong></h6>
					 <h6 style="text-align:left"><strong>2. 예외처리기능에 대해 배웁니다</strong></h6>
					 <h6 style="text-align:left"><strong>3. 예외처리기능에 대해 배웁니다</strong></h6>
				   
				</div>
				<div class="col-sm-0 col-md-0 col-lg-2" style="background-color:none;">
				</div>
            </div>
			<br>

			<!--네모 박스안에 글씨쓰기-->
			
			<!--이 강의에서 다루는 툴-->
			<br>
			<br>
			<div class="row justify-content-center">
				<div class="col-auto">
					<h4><strong>이 강의에서 다루는 툴</strong></h4>
				</div>
			</div>
			<br>
			<div class="row justify-content-center">
				<div class="col-auto" style="text-align:center">
					<h6><strong>인텔리J IDEA, 부트스트랩, 제이쿼리, 타임리프스프링<br>스프링 부트, 스프링 데이터 JPA, 스프링 시큐리티JPA, QueryDSL, PostgreSQL, JUnit</strong></h6>
				</div>
            </div>
			<br>
			<div class="row justify-content-center"><!--툴사진이 한줄에 기본 4개가 들-->
				<div class="col-sm-0 col-lg-2" style="background-color:#d6d6d6;">
					
				</div>
				<div class="col-sm-3 col-lg-2" style="background-color:#f0f0f0;height:200px">
					<class = "row justify-content-center ">
				
				</div>
				<div class="col-sm-3 col-lg-2" style="background-color:#f0f0f0;height:200px">
					<class = "row justify-content-center ">
				
				</div>
				<div class="col-sm-3 col-lg-2" style="background-color:#f0f0f0;height:200px">
					<class = "row justify-content-center ">
				
				</div>
				<div class="col-sm-3 col-lg-2" style="background-color:#f0f0f0;height:200px">
					<class = "row justify-content-center ">
				
				</div>
				<div class="col-sm-0 col-lg-2" style="background-color:#d6d6d6;">
					
				</div>
			</div>
			<br>
			
			<br>
			<br>
			<!--상세커리큘럼-->
			<div class="row justify-content-center">
				<div class="col-auto">
					<h4><strong>상세 커리큘럼</strong></h4>
				</div>
			</div>
			<br>
			
			<div class="row justify-content-center">
				<div class="col-auto" style="text-align:center">
					<h6><strong>이 과목은 이런 커리큘럼으로 진행됩니다</strong></h6>
				</div>
            </div>  
			<br><!--표-->
			<div class="row justify-content-center">
				<div class="col-sm-0 col-md-1 col-lg-2"></div>
				<div class="col-sm-12 col-md-10 col-lg-8">
					<div class="accordion" id="accordionExample">
						<div class="accordion-item">
							<h2 class="accordion-header" id="headingOne">
								<button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									서버구축
								</button>
							</h2>
							<div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
								<div class="accordion-body">
									<strong>This is the first item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headingTwo">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									홈페이지 만들기
								</button>
							</h2>
							<div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
								<div class="accordion-body">
									<strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
								</div>
							</div>
						</div>
						<div class="accordion-item">
							<h2 class="accordion-header" id="headingThree">
								<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									Accordion Item #3
								</button>
							</h2>
							<div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
								<div class="accordion-body">
									<strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
								</div>
							</div>
						</div>
					</div>
				</div> 
				<div class="col-sm-0 col-md-1 col-lg-2"></div>
			</div>
		</div>
		<br>
		
		<br>
		<br>
		<!--FAQ-->
		<div class="row justify-content-center">
				<div class="col-auto">
					<h4><strong>상세 커리큘럼</strong></h4>
				</div>
			</div>
			<br>
			
			<div class="row justify-content-center">
				<div class="col-auto" style="text-align:center">
					<h6><strong>이 과목은 이런 커리큘럼으로 진행됩니다</strong></h6>
				</div>
            </div>  
			<br>
		
	</body>
</html>