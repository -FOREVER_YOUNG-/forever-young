﻿<?  if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 
	class Project_mart_controller extends CI_controller { 
		public function coding1(){
			$this->load->view("Coding_detail_view");
		}
		public function coding2(){
			$this->load->view("Coding_detail_view_02");
		}
		//시작페이지 만들기
		public function start(){
			$this->load->view("Project_mart_view");
		}
		
		public function session_check(){
			$this->load->library("session");
			if(isset($_SESSION["id"])){
				$this->load->model("Session_login_model");//아이디를 바탕으로 회원이름을 가져오는 model기능 구현
				$result=$this->Session_login_model->getName();
				foreach ( $result->result_array() as $row ) 
					echo $row["name"]."님";

			}
			else echo "0"; 
		}
		
		//로그인 페이지 만들기
		public function login(){
			$this->load->view("Project_mart_login_view");
		}
		
	
		//로그인 검사하기 model연결
		public function loginCheck(){
			$this->load->model("Session_login_model");
			$result=$this->Session_login_model->login($_POST["id"],$_POST["pw"]);
	
			if($result->num_rows()!=1) echo "0";
			else{
				$this->load->library("session");
				$this->session->set_userdata("id",$_POST["id"]);
				echo "1";
			}
	
		}
		public function order(){
			$this->load->library("session");
			$this->load->view("Project_mart_order_list_view");
		}
		public function check_session(){
			$this->load->library("session");
			if(isset($_SESSION["id"]))
				echo "1";
			else echo "2";
		}
		public function logout(){
			$this->load->library("session");
		
			if(isset($_SESSION["id"])){
				
				unset($_SESSION["id"]);
				unset($_SESSION["pw"]);
				
					echo "로그아웃 되었습니다.";
			
				
			}
			else echo "로그인부터 하세요";
		}
	
	
	
		public function join(){
			$this->load->view("Project_mart_join_view");
		}
		public function join2(){
			$this->load->view("Session_join_view");
		}
		public function join3(){
		//여기서 한번더 검사해주면 좋긴하겠는데 내일할래
		$this->load->library("form_validation");
		$this->form_validation->set_rules("gender", "성별", "required|in_list[남자, 여자]");
		$this->form_validation->set_rules("name", "이름", "required|max_length[5]|min_length[2]");
		$this->form_validation->set_rules("email", "이메일", "required|valid_email");
		$this->form_validation->set_rules("phone_number", "폰넘버", "required");
		 
		if( $this->form_validation->run() ) {
			
			$this->load->model("session_join_model");
			
			if($this->session_join_model->join()){
				$this->load->view("Session_join_finish_view");
			}
			else {
				alert("다시시도해주십시오");
			}
		
		}
		
	
		else {
			alert("적합하지 않은 입력이 있습니다.");
		}
		
	}
		public function bag(){
		
			$this->load->library("session");
		
			if(isset($_SESSION["id"])){
				//있으면 bagview로 이동하고
			}
			else{
				//login();
				//$this->load->view("Project_mart_login_view");
				echo "로그인한 회원만 이용할 수 있습니다.\n"."로그인하시겠습니까?";

				//$this->load->view("Project_mart_login_view");
			}
		}
		
		public function delivary(){
			$this->load->library("session");
			if(isset($_SESSION["id"])){
				//있으면 delivartview로 이동하고
			}
			else{
				$this->load->view("Project_mart_login_view");
			}
		}
		
		public function road_view(){

			$this->load->view("Project_mart_road_view_view");
		}
	
			
		public function practice(){
			$this->load->view("Practice_aa");
		}
		public function dynamic_load(){
			echo "eee";
		// $this->load->view("Project_mart_view");
		}
		
		public function coffeemenu(){
			$this->load->view("Project_mart_coffeemenu_view");
		}
		public function breadmenu(){
			$this->load->view("Project_mart_breadmenu_view");
		}
		public function cakemenu(){
			$this->load->view("Project_mart_cakemenu_view");
		}
		
		public function load_view(){
			$this->load->view("Project_mart_road_view");
		}
		public function detail_view(){
			$this->load->library("session");
			$this->load->view("Project_mart_detail_view");
		}
		public function detail_view_login_check(){
			$this->load->library("session");
			if(isset($_SESSION["id"]))
				echo "1";
			else	
				echo "0";
		}
		public function option_view(){
			$this->load->view("Project_mart_option_view");
		}
		
		public $i=0;
		public $menu_order=array(array(),array(),array(),array(),array(),array(),array(),array(),array());
		public function finish_order(){
			$this->load->library("session");
			if(isset($_SESSION["id"])){
				
				/*$this->session->set_userdata("temp",$_POST["temp"]);
				$this->session->set_userdata("quantity",$_POST["quantity"]);//몇잔을 시킨건지
				$this->session->set_userdata("drink_name",$_POST["drink_name"]);
				$this->session->set_userdata("ice",$_POST["ice"]);
				$this->session->set_userdata("water",$_POST["water"]);
				$this->session->set_userdata("add_shot",$_POST["add_shot"]);
				$this->session->set_userdata("whipping",$_POST["whipping"]);
				$this->session->set_userdata("add_price",$_POST["add_price"]);
				$this->session->set_userdata("total_price",$_POST["total_price"]);*/
				$this->menu_order[$this->i][0]=$this->session->set_userdata("temp",$_POST["temp"]);
				$this->menu_order[$this->i][1]=$this->session->set_userdata("quantity",$_POST["quantity"]);//몇잔을 시킨건지
				$this->menu_order[$this->i][2]=$this->session->set_userdata("drink_name",$_POST["drink_name"]);
				$this->menu_order[$this->i][3]=$this->session->set_userdata("ice",$_POST["ice"]);
				$this->menu_order[$this->i][4]=$this->session->set_userdata("water",$_POST["water"]);
				$this->menu_order[$this->i][5]=$this->session->set_userdata("add_shot",$_POST["add_shot"]);
				$this->menu_order[$this->i][6]=$this->session->set_userdata("whipping",$_POST["whipping"]);
				$this->menu_order[$this->i][7]=$this->session->set_userdata("add_price",$_POST["add_price"]);
				$this->menu_order[$this->i][8]=$this->session->set_userdata("total_price",$_POST["total_price"]);
				$this->i++;
				echo "0";
			}
			else 
				echo "1"; 
		
		}
		public function finish_order_02(){
			$this->load->library("session");
			$this->load->view("Project_mart_order_finish_view");
		}
		public function order_detail_view(){
			$this->load->library("session");
			$this->load->view("Project_mart_order_detail_view");
		}
		public function finish_order_detail_view(){
			$this->load->library("session");
			$this->load->view("Project_mart_finish_order_detail_view");
		}
		
	}
?>