﻿<? if ( ! defined('BASEPATH') ) exit('No direct script access allowed');
 //<!--login을 검색하기위한 model을 연결하기 하기위한 controller 만들기-->
class Session_login_model_controller extends CI_controller {
	
	public function login(){

		$this->load->library("session");
		$this->load->model("session_login_model");
		$result=$this->session_login_model->login();
	
		if($result->num_rows()!=1) 
			echo "로그인실패";
		
		else {
			$this->session->set_userdata("id",$_POST["id"]);
			$this->session->set_userdata("pw",$_POST["pw"]);
			//메인페이지에 위치해놓고 로그인 성공알림창띄우기
			
			$this->load->view("Project_main_page_view");
		
		}
	}
	
	public function logout(){
		$this->load->library("session");
	
		
		if(isset($_SESSION["id"])){
			unset($_SESSION["id"]);
			unset($_SESSION["pw"]);
			echo "로그아웃 되었습니다.";
			return false;
			
		}
		else echo "로그인부터 하세요";
	}
	
	
	public function join(){
		$this->load->view("Session_join_view");

	}

 }


?>
